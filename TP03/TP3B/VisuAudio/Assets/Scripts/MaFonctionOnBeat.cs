using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaFonctionOnBeat : MonoBehaviour
{
    private Light lightComponent;
    private bool isLightOn = false;
    public EtatBeat Beat = EtatBeat.Offbeat;

    void Start()
    {
        lightComponent = GetComponent<Light>();
    }

    void Update()
    {
        // Activer ou désactiver la lumière en fonction de l'état isLightOn.
        lightComponent.enabled = isLightOn;
    }

    

    public void onOnbeatDetected()
    {
        isLightOn = !isLightOn;
    }
}
