using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisuData : MonoBehaviour
{
    public Transform Cible;
    public GameObject VisuCube;
    public float multiplier;
    
    Vector3 latloncart(float lat, float lon) 
    {
        Vector3 pos;
        float x = 0.5f * Mathf.Cos(lon) * Mathf.Cos(lat);
        float y = 0.5f * Mathf.Cos(lon) * Mathf.Sin(lat);
        float z = 0.5f * Mathf.Sin(lon);
        pos.x = 0.5f * Mathf.Cos((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);
        pos.y = 0.5f * Mathf.Sin(lat * Mathf.Deg2Rad);
        pos.z = 0.5f * Mathf.Sin((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);
        return pos;
    }

    public void VisualCube (float lat, float lon, float val, float multiplier)
    {
        GameObject cube = GameObject.Instantiate(VisuCube);
        Vector3 pos;
        pos = latloncart(lat, lon);
        cube.transform.position = new Vector3(pos.x, pos.y, pos.z);
        cube.transform.LookAt(Cible, Vector3.back);
        Vector3 echelle = cube.transform.localScale;
        echelle.z = val*multiplier;
        cube.transform.localScale = echelle;

        // Définir la couleur en fonction de l'échelle
        if (val < 1000) {
            cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(0, 255f/255f, 0);
        } else { 
            if (val < 5000) {
            cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(20f/255f, 255f/255f, 20f/255f);
            } else { 
                if (val < 10000) {
                    cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(60f/255f, 210f/255f, 0);
                } else { 
                    if (val < 50000) {
                        cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(80f/255f, 170f/255f, 0); 
                    } else {
                        if(val < 100000) {
                            cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(130f/255f, 130f/255f, 0);
                        } else {
                            if(val < 5000000) {
                                cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(170f/255f, 80f/255f, 0);
                            } else {
                                if (val < 1000000) {
                                    cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(220f/255f, 40f/255f, 0);
                                } else {
                                    cube.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(255f/255f, 0, 0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
