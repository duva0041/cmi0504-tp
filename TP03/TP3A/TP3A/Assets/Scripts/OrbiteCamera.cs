using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbiteCamera : MonoBehaviour
{

    public GameObject cible;
    public float anglesParSeconde = 45;

    // Savoir si le globe tourne ou non
    public bool clic = true;

    //Sensd de rotation du globe (vers la gauche par défaut)
    public int sens = 1;

    void Start()
    {
        this.transform.transform.position = new Vector3(2, 0, 0);
        this.transform.transform.rotation = Quaternion.Euler(0,-90f,0);

        // de base le globe tourne vers la gauche
        clic = true;
        sens = 1;
    }

    //Le but ici est de faire clique gauche pour arreter/reprendre la rotation et clique droite pour inverser le sens de rotation
    void Update()
    {
        // Si l'utilisateur fait un clic gauche ou droit de souris
        if (Input.GetMouseButtonDown(0))
        {
            // Alterne la valeur du boolean clic pour arreter ou reprendre la rotation
            Debug.Log("Clic");
            if (clic == true) {
                clic = false; //Passer d'activé à desactivé
            } else {
                clic = true; //Passer de desactivé à activé
            }
        }

        // Si l'utilisateur fait droit de souris
        if (Input.GetMouseButtonDown(1))
        {
            // Inversion du sens de rotation en alternant la valeur de sens
            if (sens == 1) {
                sens = 2; //Passer de gauche a droite
            } else {
                sens = 1; //Passer de droite a gauche
            }
        }

        // Fait tourner le globe dans le sens choisi si la rotation est activée
        if (clic == true) {
            //Tourner vers la gauche
            if (sens == 1) {
                transform.RotateAround(cible.transform.position, Vector3.up, anglesParSeconde * Time.deltaTime);
            } 
            //Tourner vers la droite
            else {
                transform.RotateAround(cible.transform.position, Vector3.up, -anglesParSeconde * Time.deltaTime);
            }
        }
    }
}
