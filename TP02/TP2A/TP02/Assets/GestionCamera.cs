using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestionCamera : MonoBehaviour
{
    public Camera camera_principale; // n1
    public Camera camera_zone;       // n2
    public Camera camera_cabine;     // n3
    public Camera camera_chariot;    // n4
    private int activeCamera;        // numero de la caméra actuellement active


    // Start is called before the first frame update
    void Start()
    {
        camera_principale.gameObject.SetActive(true);
        camera_zone.gameObject.SetActive(false);
        camera_cabine.gameObject.SetActive(false);
        camera_chariot.gameObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        // Vérifie si la touche "1" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            // Active la caméra principale et désactive les autres
            camera_principale.gameObject.SetActive(true);
            camera_zone.gameObject.SetActive(false);
            camera_cabine.gameObject.SetActive(false);
            camera_chariot.gameObject.SetActive(false);
            activeCamera = 1;
        }
        
        // Vérifie si la touche "2" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            // Active la caméra de zone et désactive les autres
            camera_principale.gameObject.SetActive(false);
            camera_zone.gameObject.SetActive(true);
            camera_cabine.gameObject.SetActive(false);
            camera_chariot.gameObject.SetActive(false);
            activeCamera = 2;
        }
        
        // Vérifie si la touche "3" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            // Active la caméra de cabine et désactive les autres
            camera_principale.gameObject.SetActive(false);
            camera_zone.gameObject.SetActive(false);
            camera_cabine.gameObject.SetActive(true);
            camera_chariot.gameObject.SetActive(false);
            activeCamera = 3;
        }
        
        // Vérifie si la touche "4" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            // Active la caméra de la moufle et désactive les autres
            camera_principale.gameObject.SetActive(false);
            camera_zone.gameObject.SetActive(false);
            camera_cabine.gameObject.SetActive(false);
            camera_chariot.gameObject.SetActive(true);
            activeCamera = 4;
        }
    }
}
