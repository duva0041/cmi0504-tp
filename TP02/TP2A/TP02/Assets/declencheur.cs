using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declencheur : MonoBehaviour
{
    //Objet pour compter le nombre de cubes dans la zone du declencheur
    private int compteur_cube = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Incrementer le compteur
        compteur_cube = compteur_cube + 1;
        Debug.Log("Ajout ; Nombre de cubes : " + compteur_cube);
    }

    private void OnTriggerExit(Collider other)
    {
        //Decrementer le compter
        compteur_cube = compteur_cube - 1;
        Debug.Log("Retrait ; Nombre de cubes : " + compteur_cube);
    }
}
