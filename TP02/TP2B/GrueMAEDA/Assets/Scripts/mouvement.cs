using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class mouvement : MonoBehaviour {

    void Start() {
    }

    void Update() {

        //Marche arriere
        if (Input.GetKey(KeyCode.DownArrow)){
            transform.Translate(Vector3.up * -0.006f);
        }

        //Lorsque l'on appuie sur la fleche pour avancer
        if (Input.GetKey(KeyCode.UpArrow)) {

            //Si on appuie sur maj droite (en plus de la fleche du haut), on sprint
            if (Input.GetKey(KeyCode.RightShift)) {
                //On va trois fois plus vite (sprint)
                transform.Translate(Vector3.up * 0.01f);
            }

            //Si on n'appuie pas sur maj droit e(marche normale)
            else {
                //Meme vitesse que la marche arriere
                transform.Translate(Vector3.up * 0.006f);
            }
        }

        //Tourner a gauche
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.Rotate(Vector3.forward, -2);
        }

        //Tourner a droite
        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Rotate(Vector3.forward, 2);
        }

    }
}