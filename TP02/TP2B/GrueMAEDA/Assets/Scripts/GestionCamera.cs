using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GestionCamera : MonoBehaviour
{
    public Camera camera_principale; // n1 Derriere la grue (comme F5 de minecraft)
    public Camera camera_zone;       // n2 Regarde le terrain (immobile)
    public Camera camera_cabine;     // n3 A l'avant de la grue (pilote)
    public Camera camera_moufle;     // n4 Descend avec la moufle


    // Start is called before the first frame update
    void Start()
    {
        camera_principale.gameObject.SetActive(true);
        camera_zone.gameObject.SetActive(false);
        camera_cabine.gameObject.SetActive(false);
        camera_moufle.gameObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        // Vérifie si la touche "1" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            // Active la caméra principale et désactive les autres
            camera_principale.gameObject.SetActive(true);
            camera_zone.gameObject.SetActive(false);
            camera_cabine.gameObject.SetActive(false);
            camera_moufle.gameObject.SetActive(false);
        }
        
        // Vérifie si la touche "2" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            // Active la caméra de zone et désactive les autres
            camera_principale.gameObject.SetActive(false);
            camera_zone.gameObject.SetActive(true);
            camera_cabine.gameObject.SetActive(false);
            camera_moufle.gameObject.SetActive(false);
        }
        
        // Vérifie si la touche "3" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            // Active la caméra de cabine et désactive les autres
            camera_principale.gameObject.SetActive(false);
            camera_zone.gameObject.SetActive(false);
            camera_cabine.gameObject.SetActive(true);
            camera_moufle.gameObject.SetActive(false);
        }
        
        // Vérifie si la touche "4" est enfoncée
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            // Active la caméra de la moufle et désactive les autres
            camera_principale.gameObject.SetActive(false);
            camera_zone.gameObject.SetActive(false);
            camera_cabine.gameObject.SetActive(false);
            camera_moufle.gameObject.SetActive(true);
        }


        // Vérifie si la touche "R" est enfoncée
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Obtient la scène actuelle
            Scene sceneActuelle = SceneManager.GetActiveScene();

            // Charge la scène actuelle pour la relancer
            SceneManager.LoadScene(sceneActuelle.name);
        }
    }
}
