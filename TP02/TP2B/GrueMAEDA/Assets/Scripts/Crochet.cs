using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crochet : MonoBehaviour
{
    public float forceCrochet = 500f;
    public ArticulationBody crochet_;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //commande pour le crochet 
        if (Input.GetKey(KeyCode.LeftShift))
        {
            crochet_.AddRelativeForce(transform.up * forceCrochet);
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            crochet_.AddRelativeForce(transform.up * -forceCrochet);
        }
    }

}