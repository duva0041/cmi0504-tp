using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class mouvement : MonoBehaviour {
    private bool entrain_de_sauter = false;

    void Start() {
    }

    void Update() {

        //Marche arriere
        if (Input.GetKey(KeyCode.DownArrow)){
            transform.Translate(Vector3.forward * 0.01f);
        }

        //Lorsque l'on appuie sur la fleche pour avancer
        if (Input.GetKey(KeyCode.UpArrow)) {

            //Si on appuie sur maj gauche ou maj droite (en plus de la fleche du haut), on sprint
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                //On va trois fois plus vite (sprint)
                transform.Translate(Vector3.back * 0.03f);
            }

            //Si on n'appuie pas sur maj (marche normale)
            else {
                //Meme vitesse que la marche arriere
                transform.Translate(Vector3.back * 0.01f);
            }
        }

        //Tourner a gauche
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.Rotate(Vector3.up, -2);
        }

        //Tourner a droite
        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Rotate(Vector3.up, 2);
        }

        //Sauter (quand on est au sol)
        if (Input.GetKey(KeyCode.Space) && !entrain_de_sauter) {
            //verifier que l'on est au sol
            entrain_de_sauter = true;

            //Saut
            transform.Translate(Vector3.up * 1f);
            
            //Timer pour arriver sur le sol
            Timer timer = new Timer(_ =>
            {
                //Fin du saut
                entrain_de_sauter = false;
            }, null, 500, Timeout.Infinite);

        }
    }
}